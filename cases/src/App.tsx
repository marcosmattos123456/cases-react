import React from 'react';
import './App.scss';
import Case1Component from './Components/case1/case1Component';
import Case2Component from './Components/case2/case2Component';
import Case3Component from './Components/case3/case3Component';
import Case4Component from './Components/case4/case4Component';
import Case5Component from './Components/case5/case5Component';
import Case6Component from './Components/case6/case6Component';
import Case7Component from './Components/case7/case7Component';
import Case8Component from './Components/case8/case8Component';
import Case9Component from './Components/case9/case9Component';
import Case10Component from './Components/case10/case10Component';
import Case11Component from './Components/case11/case11Component';

function App(props:any) {
  return (
    <div className="App">
       {props.children}
       <Case1Component />
       <Case2Component />
       <Case3Component />
       <Case4Component />
       <Case5Component />
       <Case6Component />
       <Case7Component />
       <Case8Component />
       <Case9Component />
       <Case10Component />
       <Case11Component />
    </div>
  );
}

export default App;
