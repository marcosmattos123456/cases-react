import React, { useState } from 'react';

const Case1Component = () => {

    const [displayText, setDisplayText] = useState('eu sou um bloco span')
    const [showButton, setShowButton] = useState(true)

    function clickHandler(){
        setDisplayText(displayText + ',e para de clicar porque nao vou alterar mais nada.')
        setShowButton(false)
      }

    return (
        <div className='case-item'>
            <h1>#1 Span com conteudo dinamico</h1>
            <span>{displayText}</span>
            <button hidden={!showButton} onClick={clickHandler}>Malcriado</button>
        </div >
    )
}

export default Case1Component