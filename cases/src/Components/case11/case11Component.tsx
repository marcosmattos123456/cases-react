import React, { useState } from 'react';
import './case11Component.scss';
import {FormService} from './services/case11-service';

const Case11Component = () => {

    const [inputNome, setInputNome] = useState('');
    const [nomeError, setNomeError] = useState(false);

    const [inputIdade, setInputIdade] = useState('');
    const [idadeError, setIdadeError] = useState(false);

    const [sucessMsg, setSucessMsg] = useState('')

    const resetError = (setter: Function) => {
        setter(false);
    }

    const handlerChange = (setInput: Function, key: string, event: any) => {
        let val = event.currentTarget.value
        setInput(val);
        if (key === 'nome') {
            resetError(setNomeError);
            if (val.length < 1) setNomeError(true);
        }
        if (key === 'idade') {
            resetError(setIdadeError);
            if (val.length < 1) setIdadeError(true);
        }
    }

    const sendForm = (event:any) => {
        event?.preventDefault();
        setSucessMsg('');
        
        const data = {
            nome: inputNome,
            idade: inputIdade,
        }

        FormService(data).then((res:any) => {
          console.log(res);
          setSucessMsg(res.message);
          resetError(setNomeError);
          resetError(setIdadeError);
          setInputIdade('')
          setInputNome('')
        })
      }

    return (
        <div className='case-item case11'>
            <div className='wrap'>

                <h1>#11 Forms</h1>
                <form onSubmit={sendForm.bind(true)}>
                    <input name='nome' value={inputNome} onChange={handlerChange.bind(true, setInputNome, 'nome')} placeholder='Nome' />
                    <input name='idade' value={inputIdade} onChange={handlerChange.bind(true, setInputIdade, 'idade')} placeholder="Idade" />
                    <span className='error'>
                        {nomeError ? 'Preencha o nome ' : ''}
                        {idadeError ? 'Preencha a idade ' : ''}
                    </span>

                    {sucessMsg ?
                        <span className='done'>
                            {sucessMsg}
                        </span>
                        :
                        ""
                    }
                    <input disabled={nomeError || idadeError || !inputNome || !inputIdade} type='submit' value='Enviar' />
                </form>
            </div>
        </div>
    )
}

export default Case11Component