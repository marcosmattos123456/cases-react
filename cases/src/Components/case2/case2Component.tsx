import React, { useState } from 'react';

const Case2Component = () => {

    const selectBoxItens = ['Tipo 1', 'Tipo 2', 'Tipo 3'];

    const lines = [
        { id: 0, tipo: 'Tipo 2', texto: 'um texto' },
        { id: 1, tipo: 'Tipo 1', texto: 'outro texto' }
    ]
    const [tableLines, setTableLines] = useState(lines)

    const addLine = () => {
        //se nao retorno uma nova instancia, nao reconhece a mudanca, logo, nao faz rerender
        let newTable = tableLines.concat([]); 
        newTable.push({
            id: tableLines[tableLines.length-1].id + 1,
            tipo: '',
            texto: ''
        })
        setTableLines(newTable);
      }
    
      const deleteLine = (id:number) => {
        const newTable = tableLines.filter(item =>{
          return item.id !== id;
        })
        setTableLines(newTable);
      }

      const handlerChange = (id:number, prop:string, event:any) => {
        const newTable = tableLines.map((item:any) =>{
            if(item.id === id) item[prop] = event.currentTarget.value
            return item;
        });
        setTableLines(newTable);
      }

    return (
        <div className='case-item'>
            <h1>#2 Tabela dinamica</h1>
            <table>
                {/* se eu nao coloco o thead react reclama */}
                <thead></thead> 
                <tbody>
                {tableLines.map(linha => {
                    return(
                    <tr key={linha.id}>
                        <td>
                            <select defaultValue={linha.tipo} onChange={handlerChange.bind(true,linha.id,'tipo')}>
                                <option>Selecione</option>
                                {selectBoxItens.map(item => {
                                    return(
                                        <option key={item}>{item}</option>
                                    )
                                })}
                            </select>
                        </td>
                        <td><input value={linha.texto} onChange={handlerChange.bind(true,linha.id,'texto')}/></td>
                        <td><button onClick={deleteLine.bind(true,linha.id)}>x</button></td>
                    </tr>
                    )
                })}
                </tbody>
            </table >
            <button onClick={addLine} > Adicionar linha</button >
        </div >
    )
}

export default Case2Component