import React, { useState } from 'react';
import {getData} from './service/case3-service';
import './case3-service.scss';

const Case3Component = () => {

    const [texto1, setTexto1] = useState('');
    const [texto2, setTexto2] = useState('');
    const [texto3, setTexto3] = useState('');

    const clickHandler = () => {
        getData().then((res) => {
            return res.json()
        })
        .then(res => {
            setTexto1(res.texto1)
            setTexto2(res.texto2)
            setTexto3(res.texto3)
        })
    }

    return (
        <div className='case-item case3'>
            <h1>#3 Async request</h1>
            <div className='wrap'>
                <div className='bloco'>
                    {texto1}
                </div>
                <div className='bloco'>
                    {texto2}
                </div>
                <div className='bloco'>
                    {texto3}
                </div>
            </div>
            <button onClick={clickHandler}>Traga-me esses malditos dados e um copo de gin</button>
        </div>
    )
}

export default Case3Component