import React from 'react';

const Case4Component = () => {

    const ulItens = [
        'item 1',
        'item 2',
        'item 3',
        'item 4',
        'item 5',
      ]

    return (
        <div className='case-item'>
            <h1>#4 UL dinamica</h1>
            <div className='wrap'>
                <ul>
                    {ulItens.map(item => {
                        return (
                            <li key={item}>{item}</li>
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}

export default Case4Component