import React from 'react';

const Case6Component = () => {

    return (
        <div className='case-item'>
            <h1>#6 div com gmaps</h1>
            <div className='wrap'>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2797.4023521202075!2d-73.59461328444193!3d45.48184187910116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc91a6c967bbda9%3A0xffc4d8dd414bb5bf!2sRue%20Sainte-Catherine%2C%20Qu%C3%A9bec!5e0!3m2!1sfr!2sca!4v1585404371222!5m2!1sfr!2sca" width="600" height="450" aria-hidden="false"></iframe>
            </div>
        </div>
    )
}

export default Case6Component