import React, { useState } from 'react';
import { getData } from './service/case7-service';
import './case7Component.scss';

//dentro de react, e uma pratica comum criar um componente JUNTO de outro component
//quando nao se tem a intencao de compartilhar, apenas para reuso interno
//aqui o component BtnComponent poderia sem problema algum ser um arquivo separado.

interface BtnProps {
    id: number,
    texto: string,
    fn: Function,
}

const BtnComponent = (props: BtnProps) => {
    //object descruturing do ES6
    const { fn, id, texto } = props;
    return (
        <button id={'btn' + id} onClick={fn.bind(false, id)}> {texto} </button>
    )
}

const Case7Component = () => {

    const [currentText, setCurrentText] = useState('Nenhum definido')

    const options = [
        {
            id: 0,
            text: 'comp 0'
        },
        {
            id: 1,
            text: 'comp 1'
        },
        {
            id: 2,
            text: 'comp 2'
        },
        {
            id: 3,
            text: 'comp 3'
        },
    ]

    const clickHandler = (id: number) => {
        getData()
            .then(res => res.json())
            .then(res => res.find((item: any) => item.id === id))
            .then(res => setCurrentText(res.text))
    }

    return (
        <div className='case-item case7'>
            <h1>#7 replacing data</h1>
            <div className='wrap'>
                <div className='main'>{currentText}</div>
                <div className='options'>
                    {options.map(item => {
                        return (
                            <BtnComponent key={item.id} id={item.id} texto={item.text} fn={clickHandler} />
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default Case7Component